package edu.iit.cs445.pn;

import java.io.Serializable;

public class Product implements Serializable {
	String pname;
	double price;
	Recipe r;
	int ID;
	
	public Product () {
		this.pname = "default";
		this.price = 0.00;
		this.r = new Recipe();
		this.ID = IdGenerator.newID()-1;
	}
	
	public Product (String name, double p) {
		this.pname = name;
		this.price = p;
		this.r = new Recipe();
		this.ID = IdGenerator.newID()-1;
	}

	public int getID() {
		return ID;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Recipe getRecipe() {
		return r;
	}
	
	public String getName() {
		return pname;
	}
	
	public void setName(String name) {
		this.pname = name;
	}
	
	public void updateRecipe(Recipe recipe) {
		this.r = recipe;
	}
	
	public String toString() {
		String msg = ID + ", " + pname + ", $" + price + ", " + r.toString();
		return msg;
	}
}
