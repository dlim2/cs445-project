package edu.iit.cs445.pn;

import java.io.Serializable;

public class Customer implements Serializable {
	private String name, email, phone, twitter, facebook;
	private Address a;
	private Subscription sub;
	private int ID;
	
	public Customer() {
		this.name = "Jane Doe";
		this.email = "jane.doe@example.com";
		this.phone = "1234567890";
		this.a = new Address();
		this.sub = new Subscription();
		this.ID = IdGenerator.newID();
		this.twitter = "";
		this.facebook = "";
	}
	public Customer (String name, String email, String phone, Address address) {
		this.name = name;
		this.email = email;
		this.phone = phone.replaceAll("[\\s\\-()]", ""); // drop all non-digit characters
		this.a = address;
		this.sub = new Subscription();
		this.ID = IdGenerator.newID();
	}
	private boolean isMatchName(String kw) {
		String regex = "(?i).*" + kw + ".*";
		return this.name.matches(regex);
	}
	
	private boolean isMatchEmail(String kw) {
		String regex = "(?i).*" + kw + ".*";
		return this.email.matches(regex);
	}
	
	private boolean isMatchPhone(String kw) {
		String regex = "(?i).*" + kw.replaceAll("[\\s\\-()]", "") + ".*";
		return this.phone.matches(regex);
	}
	
	public boolean isMatch(String kw) {
		if (isMatchName(kw) || isMatchEmail(kw) || isMatchPhone(kw)) {
			return true;
		} else return false;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public Subscription getSub() {
		return this.sub;
	}
	
	public void updateCustomer(String name, String email,
			String phone, Address address) {
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.a = address;
	}
	
	public void updateSocial(String twitter, String facebook) {
		this.twitter = twitter;
		this.facebook = facebook;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Address getAddress() {
		return this.a;
	}
	
	public void setAddress(Address a) {
		this.a = a;
	}
	
	public String getTwitter() {
		return this.twitter;
	}
	
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	
	public String getFacebook() {
		return this.facebook;
	}
	
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	
	public String toString() {
		String msg;
		msg = ID + ", " + name + ", " + email + ", " + phone + ", " + a.toString();
		return msg;
	}
}
