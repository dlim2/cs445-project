package edu.iit.cs445.pn;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Bakery implements Serializable {
	private final String name;
	private ArrayList<Customer> c;
	private ArrayList<Product> p;
	private ArrayList<Recipe> r;
	private int highestID;
	
	public Bakery(String name) {
		this.name = name;
		this.c = new ArrayList<Customer>();
		this.p = new ArrayList<Product>();
		this.r = new ArrayList<Recipe>();
		this.highestID = -1;
	}
	
	public void addCustomer(Customer cust) {
		boolean exists = false;
		for (Customer temp : c)
			if (temp.getEmail().equals(cust.getEmail()))
				exists = true;
		if (!exists) {
			this.c.add(cust);
			this.highestID++;
			System.out.println("Customer ID: " + cust.getID());
		}
		else
			System.out.println("User with email address " + cust.getEmail() + " already loaded.");
	}

	public ArrayList<Customer> search(String kw) {
		ArrayList<Customer> result_list = new ArrayList<Customer>();
		
		Iterator<Customer> it = this.c.iterator();
		while (it.hasNext()) {
			Customer c = it.next();
			if (c.isMatch(kw)) { 
				result_list.add(c);
			}
		}
		return result_list;
	}
	
	public int getCustomerIndex(int id) {
		int index = -1;
		for (int i = 0; i < c.size(); i++) {
			if (c.get(i).getID() == id) {
				index = i;
			}
		}
		return index;
	}
	
	public int getProductIndex (int id) {
		int index = -1;
		for (int i = 0; i < p.size(); i++) {
			if (p.get(i).getID() == id)
				index = i;
		}
		return index;
	}
	
	public int getRecipeIndex(int id) {
		int index = -1;
		for (int i = 0; i < r.size(); i++) {
			if (r.get(i).getID() == id) {
				index = i;
			}
		}
		return index;
	}
	
	public int customerCount() {
		return this.c.size();
	}
	
	public int recipeCount() {
		return this.r.size();
	}

	public void modifyCustomer(int id, String name, String email,
			String phone, Address address) {
		for (Customer cust : c) {
			if (cust.getID() == id)
				cust.updateCustomer(name, email, phone, address);
		}
	}
	
	public void modifyCustomerSocial (int id, String twitter, String facebook) {
		for (Customer cust : c) {
			if (cust.getID() == id)
				cust.updateSocial(twitter, facebook);
		}
	}

	public void addProduct(Product p1) {
		boolean exists = false;
		for (Product temp : p)
			if (temp.getRecipe().getID() == p1.getRecipe().getID())
				exists = true;
		if (!exists) {
			this.p.add(p1);
			this.highestID++;
			System.out.println("Product ID: " + p1.getID());
		}
		else
			System.out.println("Product with recipe id: " + p1.getRecipe().getID() + " already loaded.");		
	}

	public int getProductCatalogSize() {
		return p.size();
	}
	
	public String getName() {
		return this.name;
	}
	
	public ArrayList<Customer> getCustomerList() {
		return c;
	}
	
	public ArrayList<Product> getProductCatalog() {
		return p;
	}
	
	public ArrayList<Recipe> getRecipeList() {
		return r;
	}
	
	public int getLargestID() {
		return highestID;
	}

	public int totalEmptySubscriptions() {
		int count = 0;
		for (Customer cust : c) {
			if (cust.getSub().getSubscriptionCount() == 0)
				count++;
		}
		return count;
	}

	public void addProductToCustomerSubscription(int cid, int dow, int pid, int amt) {
		for (Customer cust : c) {
			if (cust.getID() == cid) {
				Product temp = new Product();
				for (Product prod : p) {
					if (prod.getID() == pid)
						temp = prod;
				}
				cust.getSub().addToSubscription(dow, temp, amt);
			}
		}
	}

	public int numberOfCustomersToDeliverTo(int dow) {
		int count = 0;
		for (Customer cust : c) {
			if (cust.getSub().getSubscriptionCountByDay(dow) > 0)
				count++;
		}
		return count;
	}
	
	public void loadCustomer(String file) {
		Customer cust = new Customer();
		String name = null, street = null, city = null, state = null, zip = null,
				email = null, phone = null, twitter = null, facebook = null;
		
		// Get the DOM Builder Factory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		// Get the DOM Builder
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			
			// Load and parse the Document
			// Document contains the complete XML as a tree
			Document document = builder.parse(file);
			
			Element rootElement = document.getDocumentElement();
			
			// Iterating through the nodes and extracting the data
			NodeList nodeList = rootElement.getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				
				if (node instanceof Element) {
					Element a = (Element) node;
					/* System.out.println("Customer " + a.getTagName() +
							": " + a.getTextContent()); */
					switch (a.getTagName()) {
					case "name":
						name = a.getTextContent();
						break;
					case "street":
						street = a.getTextContent();
						break;
					case "city":
						city = a.getTextContent();
						break;
					case "state":
						state = a.getTextContent();
						break;
					case "zip":
						zip = a.getTextContent();
						break;
					case "email":
						email = a.getTextContent();
						break;
					case "phone":
						phone = a.getTextContent();
						break;
					case "twitter":
						twitter = a.getTextContent();
						break;
					case "facebook":
						facebook = a.getTextContent();
						break;
					}
				}
			}
			Address address = new Address(street, city, state, zip);
			cust.updateCustomer(name, email, phone, address);
			cust.updateSocial(twitter, facebook);
			
			// System.out.println("Now loading: " + cust.getName());
			boolean exists = false;
			for (Customer temp : c)
				if (temp.getEmail().equals(cust.getEmail()))
					exists = true;
			this.addCustomer(cust);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void loadRecipe(String file) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(file);
			NodeList ingredientList = doc.getElementsByTagName("ingredient");
			
			Element rootElement = doc.getDocumentElement();
			NodeList nodeList = rootElement.getChildNodes();
			String rName = null, instructions = null, unitsMade = null;
			
			//System.out.println("nodeList length: " + nodeList.getLength());
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node recipeInfo = nodeList.item(i);
				if (recipeInfo instanceof Element) {
					Element rInfo = (Element) recipeInfo;
					/* System.out.println("Recipe " + rInfo.getTagName() +
							": " + rInfo.getTextContent()); */
					switch (rInfo.getTagName()) {
					case "name":
						rName = rInfo.getTextContent();
						break;
					case "instructions":
						instructions = rInfo.getTextContent();
						break;
					case "units_made":
						unitsMade = rInfo.getTextContent();
						break;
					}
				}
			}
			//System.out.println(rName + "\n" + instructions + "\n" + unitsMade);
			Recipe recipe = new Recipe(rName, instructions, Integer.parseInt(unitsMade));
			
			//System.out.println("ingredientList length: " + ingredientList.getLength());
			for (int i = 0; i < ingredientList.getLength(); i++) {
				Node ingredient = ingredientList.item(i);
				String name = null, amount = null, unit = null, type = null;
				if (ingredient instanceof Element) {
					Element ing = (Element) ingredient;
					NodeList infoList = ing.getChildNodes();
					//System.out.println("infoList length: " + infoList.getLength());
					for (int j = 0; j < infoList.getLength(); j++) {
						Node info = infoList.item(j);
						if (info instanceof Element) {
							Element inf = (Element) info;
							/*System.out.println("Ingredient " + inf.getTagName() + ": " + 
									inf.getTextContent());*/
							switch (inf.getTagName()) {
							case "name":
								name = inf.getTextContent();
								//System.out.println("Parse name!");
								break;
							case "amount":
								amount = inf.getTextContent();
								//System.out.println("Parse amount!");
								break;
							case "measuring_unit":
								unit = inf.getTextContent();
								//System.out.println("Parse unit!");
								break;
							case "type":
								type = inf.getTextContent();
								//System.out.println("Parse type!");
								break;
							}
						}
					}
				}
				Ingredient ingred = new Ingredient(name, Double.parseDouble(amount), unit, type);
				//System.out.println(ingred.toString());
				recipe.addIngredient(ingred);
			}
			boolean exists = false;
			for (Recipe temp : r)
				if (temp.getName().equals(recipe.getName()))
					exists = true;
			if (!exists){
				r.add(recipe);
				this.highestID++;
				System.out.println("Recipe ID: " + recipe.getID());
			}
			else
				System.out.println("Recipe with name " + recipe.getName() + " already loaded.");
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
