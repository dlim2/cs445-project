package edu.iit.cs445.pn;

import java.util.ArrayList;
import java.util.Collection;
import java.io.*;

public class TestWrapper {

	public static void testPasses(String msg) {
		System.out.println("pass: " + msg);
	}
	
	public static void testFails(String msg) {
		System.out.println("fail: " + msg);
	}
	
	public static void saveBakeryState(Bakery b) throws IOException {
		FileOutputStream fout = null;
		ObjectOutputStream oos = null;
		try {
			fout = new FileOutputStream("tmp/bakery.ser");
			oos = new ObjectOutputStream(fout);
			oos.writeObject(b);
		} catch (IOException e) {
		        e.printStackTrace();
		} finally {
			if(oos != null) {
				oos.close();
			} 
		}
	}
	
	public static Bakery restoreBakeryState(Bakery b) throws IOException {
		ObjectInputStream ois = null;
		try {
			FileInputStream fis = new FileInputStream("tmp/bakery.ser");
			ois = new ObjectInputStream(fis);
			b = (Bakery)ois.readObject();
			return b;
		} catch (IOException e) {
			System.err.println("Nothing to restore.\n");
		} 
		catch (ClassNotFoundException e) {
			System.err.println("ClassNotFoundException caught in restoreBakeryState()");
			e.printStackTrace();
		}
			finally {
			if (ois != null) {
				ois.close();
			} 
		}
		return b;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String m;		
		Bakery b = new Bakery("Panem Nostrum");
		Customer c1 = new Customer();
		Customer c2 = new Customer("John Doe", "johnd@example.com", "(901) 234-5678", new Address());
		Collection<Customer> search_results;
		
		b.addCustomer(c1);
		b.addCustomer(c2);
		
// Test customer count
		m = "Test the number of customers";
		if (b.customerCount() == 2) testPasses(m); else testFails(m);
		
// Test search		
		search_results = b.search("virgil");
		m = "Search for non-existent customer";
		if (search_results.size() == 0) testPasses(m); else testFails(m);
		
		search_results = b.search("jane");
		m = "Search for existent customer";
		if (search_results.size() == 1) testPasses(m); else testFails(m);
		
		search_results = b.search("456-789");
		m = "Search for existent formatted phone number";
		if (search_results.size() == 1) testPasses(m); else testFails(m);
// Test modify customer
		b.modifyCustomer(c2.getID(), "John Doe", "johnny.doe@example.com", "(901) 234-5678", new Address());
		search_results = b.search("Johnny");
		m = "Search for modified customer";
		if (search_results.size() == 1) testPasses(m); else testFails(m);
		
// Test add products to product catalog
		Product p1 = new Product();
		Product p2 = new Product("9 Grains", 4.95);
		b.addProduct(p1);
		b.addProduct(p2);
		m = "Test the number of products in the product catalog";
		if (b.getProductCatalogSize() == 2) testPasses(m); else testFails(m);
		
//Test subscriptions		
		m = "Test the total number of empty subscriptions";
		if(b.totalEmptySubscriptions() == 2) testPasses(m); else testFails(m);
		
		m = "Test adding to a customer subscription";
		b.addProductToCustomerSubscription(c1.getID(), 0, p1.getID(), 1);
		if(b.totalEmptySubscriptions() == 1) testPasses(m); else testFails(m);
		
		m = "Test adding a second entry to customer subscription";
		b.addProductToCustomerSubscription(c1.getID(), 1, p2.getID(), 2);
		if(b.totalEmptySubscriptions() == 1) testPasses(m); else testFails(m);
// Add some entries to the subscription for Customer c2		
		b.addProductToCustomerSubscription(c2.getID(), 3, p1.getID(), 3);
		b.addProductToCustomerSubscription(c2.getID(), 1, p1.getID(), 4);
// Test the number of customers to deliver to by day		
		m = "Test number of customers to deliver to on Sunday";
		if (b.numberOfCustomersToDeliverTo(0) == 1) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Monday";
		if (b.numberOfCustomersToDeliverTo(1) == 2) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Tuesday";
		if (b.numberOfCustomersToDeliverTo(2) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Wednesday";
		if (b.numberOfCustomersToDeliverTo(3) == 1) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Thursday";
		if (b.numberOfCustomersToDeliverTo(4) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Friday";
		if (b.numberOfCustomersToDeliverTo(5) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Saturday";
		if (b.numberOfCustomersToDeliverTo(6) == 0) testPasses(m); else testFails(m);

// Try saving bakery state
		try {
			saveBakeryState(b);
		} catch (IOException e) {
			System.err.println("IOException caught when trying to save state.\n");
		}
		
// Try restoring bakery state
		b = null;
		try {
			b = restoreBakeryState(b);
			System.out.println("Bakery restored successfully");
		} catch (IOException e) {
			System.err.println("IOException caught when trying to restore state");
		}

// Test customer count
		m = "Test the number of customers";
		if (b.customerCount() == 2) testPasses(m); else testFails(m);
		
// Test search		
		search_results = b.search("virgil");
		m = "Search for non-existent customer";
		if (search_results.size() == 0) testPasses(m); else testFails(m);
		
		search_results = b.search("jane");
		m = "Search for existent customer";
		if (search_results.size() == 1) testPasses(m); else testFails(m);
		
		search_results = b.search("456-789");
		m = "Search for existent formatted phone number";
		if (search_results.size() == 1) testPasses(m); else testFails(m);
				
//Test subscriptions		
		m = "Test the total number of empty subscriptions";
		if(b.totalEmptySubscriptions() == 0) testPasses(m); else testFails(m);
		
// Test the number of customers to deliver to by day		
		m = "Test number of customers to deliver to on Sunday";
		if (b.numberOfCustomersToDeliverTo(0) == 1) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Monday";
		if (b.numberOfCustomersToDeliverTo(1) == 2) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Tuesday";
		if (b.numberOfCustomersToDeliverTo(2) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Wednesday";
		if (b.numberOfCustomersToDeliverTo(3) == 1) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Thursday";
		if (b.numberOfCustomersToDeliverTo(4) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Friday";
		if (b.numberOfCustomersToDeliverTo(5) == 0) testPasses(m); else testFails(m);
		m = "Test number of customers to deliver to on Saturday";
		if (b.numberOfCustomersToDeliverTo(6) == 0) testPasses(m); else testFails(m);

		
	}
}