package edu.iit.cs445.pn;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void test_search_match_name() {
		Customer c = new Customer();
		assertTrue(c.isMatch("jane"));
	}
	
	@Test
	public void test_search_fails() {
		Customer c = new Customer();
		assertFalse(c.isMatch("smith"));
	}
	
	@Test
	public void test_search_match_email() {
		Customer c = new Customer();
		assertTrue(c.isMatch("Jane"));
	}	
	
	@Test
	public void test_search_match_phone_partial() {
		Customer c = new Customer();
		assertTrue(c.isMatch("7890"));
	}
	
	@Test
	public void test_search_match_phone_full() {
		Customer c = new Customer("John Doe", "johndoe@example.com", "(123) 456-7890", new Address());
		assertTrue(c.isMatch("1234567890"));
	}
}
