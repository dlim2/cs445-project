package edu.iit.cs445.pn;

import java.io.Serializable;

public class Address implements Serializable{
	String street, apt, city, state, zip;
	
	public Address() {
		this.street = "123 Main ST";
		this.apt = "";
		this.city = "Anytown";
		this.state = "Anystate";
		this.zip = "12345";
	}
	public Address(String st, String ci,
			String state, String zip) {
		this.street = st;
		this.city = ci;
		this.state = state;
		this.zip = zip;
	}
	public void addAptNum(String apt) {
		this.apt = apt;
	}
	
	public void updateAddress(String street, String city, String state, String zip) {
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String toString() {
		String msg;
		msg = street + ", " + city + ", " + state + ", " + zip;
		return msg;
	}
}
