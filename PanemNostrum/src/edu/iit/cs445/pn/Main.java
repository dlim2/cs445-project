package edu.iit.cs445.pn;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Bakery b = new Bakery("Panem Nostrum");
		ArrayList<Customer> list = null;
		Scanner scan = new Scanner(System.in);
		String msg;
		
		b.addCustomer(new Customer());
		list = b.search("virgil");	// this should return a result set, all customers that match the search keyword
		if (list.size() > 0)
			for (Customer cust: list) {
				cust.toString();
			}
		else
			System.out.println("There are no matches...");
		// do something with the result set
		while (true) {
			System.out.print("$: ");
			msg = scan.nextLine();
			
			if (msg.equalsIgnoreCase("quit")){
				System.out.println("Goodbye");
				System.exit(0);
			}
			else if (msg.equalsIgnoreCase("load")) {
				System.out.println("Which file to load?: ");
				String filename = scan.nextLine();
				b.loadCustomer(filename);
			}
			else if (msg.equalsIgnoreCase("count")) {
				System.out.println(b.customerCount());
			}
			else if (msg.equalsIgnoreCase("search")) {
				System.out.println("Type name to search: ");
				String name = scan.nextLine();
				ArrayList <Customer> results = b.search(name);
				for (Customer cust : results) {
					System.out.println(cust.toString());
				}
			}
			else
				System.out.println("echo: " + msg);
		}
	}
}
