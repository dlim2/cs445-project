package edu.iit.cs445.pn;

import java.io.Serializable;

public class Ingredient implements Serializable{

	private String name;
	private double amount;
	private String unit;
	private String type;
	
	public Ingredient() {
		this.name = "Default";
		this.amount = 0.0;
		this.unit = "Default";
		this.type = "Default";
	}
	
	public Ingredient(String name, double amount, String unit, String type) {
		this.name = name;
		this.amount = amount;
		this.unit = unit;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public String getType() {
		return type;
	}
	
	public String toString() {
		String msg = "Ingredient name: " + this.name +
					 "\nAmount: " + this.amount +
					 "\nUnit: " + this.unit +
					 "\nType: " + this.type;
		return msg;
	}
}
