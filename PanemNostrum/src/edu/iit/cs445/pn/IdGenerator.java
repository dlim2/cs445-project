package edu.iit.cs445.pn;

import java.util.concurrent.atomic.AtomicInteger;

public final class IdGenerator {
	private static AtomicInteger nextID = new AtomicInteger();
	
	public static int newID() {
		return nextID.getAndIncrement();
	}
	public static void setID(Bakery b) {
		nextID.set(b.getLargestID()+1);
	}
}
