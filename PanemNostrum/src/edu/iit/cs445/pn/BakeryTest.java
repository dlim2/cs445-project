package edu.iit.cs445.pn;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class BakeryTest {

	
	
	@Test
	public void test_add_customer() {
		Bakery b = new Bakery("Test bakery");
		
		b.addCustomer(new Customer());
		assertEquals(1, b.customerCount(), 0.01);
		
		b.addCustomer(new Customer("John Don", "jdon@example.com", "0987654321", new Address()));
		assertEquals(2, b.customerCount(), 0.01);
	}
	
	@Test
	public void test_find_customer() {
		Bakery b = new Bakery("Test bakery");
		b.addCustomer(new Customer());
		ArrayList <Customer> custlist = b.search("jane");
		
		assertEquals(1, custlist.size(), 0.01);
		assertEquals("Name Result = Jane Doe", "Jane Doe", custlist.get(0).getName());
		
	}
	
	@Test
	public void test_load_customer() {
		Bakery b = new Bakery("Test bakery");
		b.loadCustomer("xml/Customer1.xml");
		ArrayList <Customer> custlist = b.search("");
		
		assertEquals(1, b.customerCount(), 0.01);
		assertEquals("Name of Customer1", "Jane Roe", custlist.get(0).getName());
	}
	
	@Test
	public void test_load_recipe() {
		Bakery b = new Bakery("Test Bakery");
		b.loadRecipe("xml/Recipe1.xml");
		b.loadRecipe("xml/Recipe2.xml");
		ArrayList<Recipe> rList = b.getRecipeList();
		
		
		
	}

}
