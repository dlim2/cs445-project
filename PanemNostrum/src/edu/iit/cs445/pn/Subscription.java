package edu.iit.cs445.pn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;


public class Subscription implements Serializable {
	private class DailyDelivery implements Serializable {
		int d;	    // 0=Sunday, 6=Saturday
		Product p;	// product to deliver
		int q;		// units of product to deliver
		
		public DailyDelivery(int dow, Product prod, int amt) {
			this.d = dow;
			this.p = prod;
			this.q = amt;
		}
		
		public boolean dayMatch(int dow) {
			if (this.d == dow) 
				return true;
			else 
				return false;
		}
	}
	
	private ArrayList<DailyDelivery> dd = new ArrayList<DailyDelivery>();
	
	public void addToSubscription(int dow, Product p, int amt) {	// dow = "day of week"
		dd.add(new DailyDelivery(dow, p, amt));
	}
	
	public int getSubscriptionCount() {
		return dd.size();
	}

	public int getSubscriptionCountByDay(int dow) {
		int daysMatching=0;
		Iterator<DailyDelivery> it = dd.iterator();
		while(it.hasNext()) {
			DailyDelivery aday = it.next();
			if (aday.dayMatch(dow)) {
				daysMatching++;
			}
		}
		return daysMatching;
	}
	
}
