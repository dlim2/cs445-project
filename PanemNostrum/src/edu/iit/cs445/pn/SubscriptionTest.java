package edu.iit.cs445.pn;

import static org.junit.Assert.*;
import org.junit.Test;

public class SubscriptionTest {
	private static Product p0 = new Product("Baguette", 1.0);
	private static Product p1 = new Product("9 grains", 1.1);
	private static Product p2 = new Product("Rustic potato", 1.2);
	private static Product p3 = new Product("Whole wheat", 1.3);
	private static Product p4 = new Product("Roll", 1.4);
	
	private void setupSubscription(Subscription s) {
		s.addToSubscription(0, p0, 1);
		s.addToSubscription(0, p4, 1);
		s.addToSubscription(1, p1, 1);
		s.addToSubscription(1, p4, 2);
		s.addToSubscription(2, p2, 1);
		s.addToSubscription(2, p4, 2);
		s.addToSubscription(3, p3, 1);
		s.addToSubscription(3, p3, 3);
		s.addToSubscription(4, p0, 1);
		s.addToSubscription(4, p4, 4);
		s.addToSubscription(5, p0, 1);
		s.addToSubscription(5, p4, 5);
		s.addToSubscription(6, p1, 1);
		s.addToSubscription(6, p4, 6);
	}

	@Test
	public void test_add_one_product_to_new_subscription() {
		Subscription s = new Subscription();
		s.addToSubscription(0, p0, 10);
		assertEquals(1, s.getSubscriptionCount(), 0.01);
	}
	
	@Test 
	public void test_add_two_products_to_new_subscription() {
		Subscription s = new Subscription();
		setupSubscription(s);
		assertEquals(14, s.getSubscriptionCount(), 0.01);
	}
	
	@Test
	public void test_subscription_count_by_day() {
		Subscription s = new Subscription();
		setupSubscription(s);
		assertEquals(2, s.getSubscriptionCountByDay(1), 0.01);
	}

}
