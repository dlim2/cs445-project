package edu.iit.cs445.pn;

import java.io.Serializable;
import java.util.ArrayList;

public class Recipe implements Serializable {
	private int id;
	private String name, instructions;
	private int unitsMade;
	private ArrayList <Ingredient> ingredients;
	
	public Recipe() {
		this.id = IdGenerator.newID();
		this.name = "Default";
		this.instructions = "None";
		this.ingredients = new ArrayList<Ingredient>();
		this.unitsMade = 0;
	}
	
	public Recipe(String name, String instructions, int unitsMade) {
		this.id = IdGenerator.newID();
		this.name = name;
		this.instructions = instructions;
		this.ingredients = new ArrayList<Ingredient>();
		this.unitsMade = unitsMade;
	}
	
	public void addIngredient(Ingredient ing) {
		this.ingredients.add(ing);
	}
	
	public void update(String name, String instructions, ArrayList <Ingredient> ingredients, int unitsMade) {
		this.name = name;
		this.instructions = instructions;
		this.ingredients = ingredients;
		this.unitsMade = unitsMade;
	}
	
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public int getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getInstructions() {
		return instructions;
	}
	
	public int getUnitsMade() {
		return unitsMade;
	}
	
	public void viewIngredients() {
		for (Ingredient in : this.ingredients) {
			System.out.println(in.toString());
		}
	}
	
	public String toString() {
		String msg = "recipe: " + name + " (id=" + id + ")";
		return msg;
	}
}
